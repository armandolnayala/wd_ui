import { Pipe, PipeTransform } from "@angular/core";
import * as internal from "assert";

@Pipe({
  name: "textSubstring",
})
export class TextSubstringPipe implements PipeTransform {
  transform(value: string, start: number, end: number, sufix: string): string {
    if (value == null) {
      return value;
    }

    if (value.length > end) {
      var subText = value.substring(start, end);
      if (sufix) {
        return subText + sufix;
      }

      return subText;
    } else {
      return value.substring(start, end);
    }
  }
}
