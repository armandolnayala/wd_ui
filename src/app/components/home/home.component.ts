import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ClipboardService } from "ngx-clipboard";

//Services
import { UserService } from "../../services/user.service";
import { AccesslinkService } from "../../services/accesslink.service";
import { AlertService } from "../../services/alert.service";
import { UtilService } from "../../services/util.service";
import { ConfirmationdialogService } from "../../support/services/confirmationdialog.service";
import { CONSTANT } from "../../services/constant";

//Models
import {
  PageFilter,
  QueryFilter,
  GenericFilter,
} from "../../models/generic-filter";
import { IndexModel } from "../../models/ui/index-model";
import { GenericResponse } from "../../models/genericresponse";
import { OperationResult } from "../../models/operationresult";
import { Accesslink } from "../../models/accesslink";
import { AccesslinkCreate } from "../../models/ui/accesslinkcreate";
import { Alert } from "../../models/ui/alertui";
import { ConfirmationdialogModel } from "../../support/models/ConfirmationdialogModel";
import { Status } from "../../models/status";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit {
  public title: string;
  public genericResponse: GenericResponse;
  public accesslinks: Accesslink[] = [];
  public operationResult: OperationResult;
  public accessLinkEntity: AccesslinkCreate;
  private modalRef;

  public indexModel: IndexModel<Accesslink> = new IndexModel();
  public queryFilter: QueryFilter;
  public filter: GenericFilter;
  public pageFilter: PageFilter;

  constructor(
    private translate: TranslateService,
    private _userService: UserService,
    private _accessLinkService: AccesslinkService,
    private _alertService: AlertService,
    private _utilService: UtilService,
    private _confirmationdialogService: ConfirmationdialogService,
    private modalService: NgbModal,
    private _clipboardService: ClipboardService
  ) {
    this.title = "page.title_access_link";

    this.queryFilter = { status: null, query: null, extras: [] };
    this.filter = new GenericFilter(this.queryFilter, { name: "asc" });
    this.pageFilter = new PageFilter(0, CONSTANT.page_limit);

    this.operationResult = new OperationResult(null, "", false);
    this.accessLinkEntity = new AccesslinkCreate("", "", "", false);
  }

  ngOnInit(): void {
    this.translate.use(this._userService.getLang());
    this.loadAccessLinks();
  }

  loadAccessLinks() {
    this.operationResult.inProgress = true;
    this._accessLinkService
      .findAll(this.filter, this.pageFilter.page, this.pageFilter.limit)
      .subscribe(
        (response) => {
          this.operationResult =
            this._utilService.processGenericResponse(response);

          if (!this.operationResult.error) {
            this.indexModel.result =
              this.operationResult.genericResponse.data.result;
            this.indexModel.hasMore =
              this.operationResult.genericResponse.data.hasMore;
            this.indexModel.count =
              this.operationResult.genericResponse.data.count;
          }
        },
        (httpError) => {
          this._utilService.processHttpError(httpError, null);
        }
      );
  }

  reloadResults() {
    this.filter.filter = this.queryFilter;
    this.loadAccessLinks();
  }

  onSelectedPageEvent(event) {
    this.pageFilter.page = event.page;
    this.reloadResults();
  }

  onSelectedPageLimitOptionEvent(event) {
    this.pageFilter.limit = event.pageLimit;
    this.reloadResults();
  }

  showSection(name: string): Boolean {
    switch (name) {
      case "not_found_results":
        return !this.indexModel.result || this.indexModel.result.length == 0;
      case "results":
        return this.indexModel.result && this.indexModel.result.length > 0;
      case "loading":
        return this.operationResult.inProgress;
      default:
        return false;
    }
  }

  cleanAccessLinkEntity() {
    this.accessLinkEntity.id = null;
    this.accessLinkEntity.title = "";
    this.accessLinkEntity.url = "";
    this.accessLinkEntity.description = "";
    this.accessLinkEntity.isNotValid = false;
    this.accessLinkEntity.wdReadOnly = false;
  }

  openCreateModal(content, item) {
    if (item) {
      this.accessLinkEntity.id = item._id;
      this.accessLinkEntity.title = item.title;
      this.accessLinkEntity.url = item.url;
      this.accessLinkEntity.description = item.description;
    }

    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
    });

    this.modalRef.result.then(
      (result) => {
        this.cleanAccessLinkEntity();
      },
      (reason) => {
        this.cleanAccessLinkEntity();
      }
    );
  }

  saveAccessLink() {
    var accessLinkToSave = Accesslink.instanceToSave(
      this.accessLinkEntity.title,
      this.accessLinkEntity.url,
      this.accessLinkEntity.description,
      true
    );

    this._accessLinkService.createAccessLink(accessLinkToSave).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this.modalRef.close(this.accessLinkEntity);
          this._alertService.showAlert(
            new Alert("general.message_action_success", "success")
          );

          this.loadAccessLinks();
        } else {
          this.modalRef.dismiss("close");
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, (data) => {
          this.modalRef.dismiss("close");
        });
      }
    );
  }

  updateAccessLink() {
    var accessLinkToSave = Accesslink.instanceToSave(
      this.accessLinkEntity.title,
      this.accessLinkEntity.url,
      this.accessLinkEntity.description,
      true
    );

    this._accessLinkService
      .updateAccessLink(this.accessLinkEntity.id, accessLinkToSave)
      .subscribe(
        (response) => {
          this.operationResult =
            this._utilService.processGenericResponse(response);

          if (!this.operationResult.error) {
            this.modalRef.close(this.accessLinkEntity);
            this._alertService.showAlert(
              new Alert("general.message_action_success", "success")
            );

            this.loadAccessLinks();
          } else {
            this.modalRef.dismiss("close");
            this._alertService.showAlert(
              new Alert(this.operationResult.message, "danger")
            );
          }
        },
        (httpError) => {
          this._utilService.processHttpError(httpError, (data) => {
            this.operationResult = data;
            this.modalRef.dismiss("close");
          });
        }
      );
  }

  closeCreateModel(reason) {
    if (reason === "SAVE" && this.validateCreateAccessLink()) {
      if (this.accessLinkEntity.id != null) {
        this.updateAccessLink();
      } else {
        this.saveAccessLink();
      }
    }

    if (reason === "CLOSE") {
      this.modalRef.dismiss("close");
    }
  }

  validateCreateAccessLink() {
    if (!this.accessLinkEntity.title || !this.accessLinkEntity.url) {
      this.accessLinkEntity.isNotValid = true;
      return false;
    }

    this.accessLinkEntity.isNotValid = false;
    return true;
  }

  public openConfirmationDialog(accessLinkId) {
    this._confirmationdialogService
      .confirm(ConfirmationdialogModel.defaultDialog())
      .then((confirmed) => {
        if (confirmed == true) {
          this.deleteAccessLink(accessLinkId);
        }
      })
      .catch(() => {});
  }

  private deleteAccessLink(accessLinkId: string) {
    this._accessLinkService.deleteAccessLink(accessLinkId).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this._alertService.showAlert(
            new Alert("general.message_action_success", "success")
          );

          this.loadAccessLinks();
        } else {
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, null);
      }
    );
  }

  openExternalLink(url: string) {
    if (url) {
      window.open(url, "_blank");
    }
  }

  onSearchKeyDown(e: any) {
    if (e.keyCode == CONSTANT.key_code.enter && e.ctrlKey) {
      this.processOnSearchKeyDown(true);
    }
  }

  processOnSearchKeyDown(openUrl) {
    if (this.indexModel.result && this.indexModel.result.length > 0) {
      if (
        openUrl &&
        this.indexModel.result[0].url &&
        this.indexModel.result[0].url != null
      ) {
        this.openExternalLink(this.indexModel.result[0].url);
      }
    }
  }

  onCopy(item) {
    let itemToCopy = {
      title: item.title,
      url: item.url,
      description: item.description,
    };

    this._clipboardService.copy(JSON.stringify(itemToCopy));
    this._alertService.showAlert(
      new Alert("general.message_copied", "success")
    );
  }
}
