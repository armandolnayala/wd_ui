import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

//Services
import { UserService } from "../../services/user.service";
import { CONSTANT } from "../../services/constant";
import { AlertService } from "../../services/alert.service";
import { UtilService } from "../../services/util.service";
import { ConfirmationdialogService } from "../../support/services/confirmationdialog.service";

//Models
import {
  UpdateUser,
  UpdateDataUser,
  UpdateUserUI,
} from "../../models/updateuser";
import { GenericResponse } from "../../models/genericresponse";
import { OperationResult } from "../../models/operationresult";
import { Alert } from "../../models/ui/alertui";
import { ConfirmationdialogModel } from "../../support/models/ConfirmationdialogModel";

@Component({
  selector: "app-edituser",
  templateUrl: "./edituser.component.html",
  styleUrls: ["./edituser.component.css"],
})
export class EdituserComponent implements OnInit {
  public genericResponse: GenericResponse;
  public operationResult: OperationResult;
  public updateUser: UpdateUser;
  public model: UpdateUserUI;
  public locales = [];
  public title: string;
  public wdDeleteAccountModel = {
    wdDataKey: null,
    isNotValid: false,
    message: null,
  };
  private modalRef;

  constructor(
    private _userService: UserService,
    private _translate: TranslateService,
    private _alertService: AlertService,
    private _utilService: UtilService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _confirmationdialogService: ConfirmationdialogService,
    private modalService: NgbModal
  ) {
    this.title = "createuser.title_edit_form";

    this.updateUser = new UpdateUser();
    this.updateUser.user = new UpdateDataUser();
    this.model = new UpdateUserUI();
    this.operationResult = new OperationResult(null, "", false);

    this.model.locale = CONSTANT.locales[0].lang;
    this.model.locales = CONSTANT.locales;
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.model.id = this._route.snapshot.paramMap.get("id");

    if (!this.model.id) {
      this._alertService.showAlert(
        new Alert("general.message_id_empty", "danger")
      );
      this._router.navigate(["/"]);
    } else {
      this.loadData();
    }
  }

  loadData() {
    this.operationResult.inProgress = true;
    this._userService.findById(this.model.id).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this.model.name = this.operationResult.genericResponse.data.name;
          this.model.surname =
            this.operationResult.genericResponse.data.surname;
          this.model.locale = this.operationResult.genericResponse.data.locale;
          this.model.email = this.operationResult.genericResponse.data.email;
        } else {
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
          this._router.navigate(["/"]);
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, (data) => {
          this._router.navigate(["/"]);
        });
      }
    );
  }

  executingAction(name: string): Boolean {
    switch (name) {
      case "onDeleteAccount":
        return this.operationResult.inProgress;
      case "onCancel":
        return this.operationResult.inProgress;
      case "onCancelDeleteAccount":
        return this.operationResult.inProgress;
      case "onAcceptDeleteAccount":
        return this.operationResult.inProgress;
      default:
        return false;
    }
  }

  onUpdatePassword(event) {
    this.model.updatePassword = event.target.checked;
  }

  confirmationAction() {
    this._confirmationdialogService
      .confirm(ConfirmationdialogModel.defaultDialog())
      .then((confirmed) => {
        if (confirmed == true) {
          this.onSubmit();
        }
      })
      .catch(() => {});
  }

  cancelAction() {
    this._router.navigate(["/"]);
  }

  validateSubmit() {
    if (this.model.updatePassword) {
      if (!this.model.currentPassword) {
        this._alertService.showAlert(
          new Alert("createuser.error_required_current_password", "danger")
        );

        return false;
      }

      if (!this.model.password) {
        this._alertService.showAlert(
          new Alert("login.error_required_password", "danger")
        );

        return false;
      }

      if (!this.model.confirmPassword) {
        this._alertService.showAlert(
          new Alert("createuser.error_required_confirm_pass", "danger")
        );

        return false;
      }
    }

    return true;
  }

  onSubmit() {
    this.operationResult.inProgress = true;

    this.updateUser.user.name = this.model.name;
    this.updateUser.user.surname = this.model.surname;
    this.updateUser.user.locale = this.model.locale;
    this.updateUser.user.password = this.model.password;
    this.updateUser.updatePassword = this.model.updatePassword;
    this.updateUser.currentPassword = this.model.currentPassword;

    this._userService.updateUser(this.updateUser).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this._userService.updateIdentity(
            this.operationResult.genericResponse.data
          );

          this._alertService.showAlert(
            new Alert("general.message_action_success", "success")
          );
          this._router.navigate(["/"]);
        } else {
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, (data) => {
          this.operationResult = data;
        });
      }
    );
  }

  wdDeleteAccountClean() {
    this.wdDeleteAccountModel.wdDataKey = null;
    this.wdDeleteAccountModel.isNotValid = false;
    this.wdDeleteAccountModel.message = null;
  }

  openModalDeleteAccount(content) {
    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
    });

    this.modalRef.result.then(
      (result) => {
        this.wdDeleteAccountClean();
      },
      (reason) => {
        this.wdDeleteAccountClean();
      }
    );
  }

  onActionDeleteAccountModal(reason) {
    if (reason === "ACCEPT" && this.validateDeleteAccount()) {
      this.wdDeleteAccountModel.message=null;
      this.onDeleteAccount();
    }

    if (reason === "CLOSE") {
      this.modalRef.dismiss("close");
    }
  }

  validateDeleteAccount() {
    if (!this.wdDeleteAccountModel.wdDataKey) {
      this.wdDeleteAccountModel.isNotValid = true;
      return false;
    }

    this.wdDeleteAccountModel.isNotValid = false;
    return true;
  }

  onDeleteAccount() {
    this.operationResult.inProgress = true;
    this._userService
      .deleteAccount(this.wdDeleteAccountModel.wdDataKey)
      .subscribe(
        (response) => {
          this.operationResult =
            this._utilService.processGenericResponse(response);

          if (!this.operationResult.error) {
            this.modalRef.dismiss("close");
            this._alertService.showAlert(
              new Alert("createuser.message_success_delete_account", "success")
            );
            this._userService.logout();
            this._router.navigate(["/login"]);
          } else {
            this._alertService.showAlert(
              new Alert(this.operationResult.message, "danger")
            );
            this.wdDeleteAccountModel.message = this.operationResult.message;
          }
        },
        (httpError) => {
          this._utilService.processHttpError(httpError, (data) => {
            this.operationResult = data;
            this.wdDeleteAccountModel.message = this.operationResult.message;
            //this.modalRef.dismiss("close");
          });
        }
      );
  }
}
