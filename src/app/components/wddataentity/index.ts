import { WddataIndexComponent } from "./wddata-index/wddata-index.component";

export const WDDATAENTITY_COMPONENTS = [WddataIndexComponent];

export { WddataIndexComponent } from "./wddata-index/wddata-index.component";

export { WdDataEntityRoutingModule } from "./wddataentity-routing.module";
