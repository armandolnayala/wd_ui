import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { WddataIndexComponent } from "./wddata-index/wddata-index.component";

import { AuthGuard } from "../../services/auth.guard";

const wdDataEntityRoutes: Routes = [
  {
    path: "wddataentity",
    canActivate: [AuthGuard],
    children: [
      { path: "", redirectTo: "index", pathMatch: "full" },
      { path: "index", component: WddataIndexComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(wdDataEntityRoutes)],
  exports: [RouterModule],
})
export class WdDataEntityRoutingModule {}
