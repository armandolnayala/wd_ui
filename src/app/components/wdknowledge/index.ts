import { WdknowIndexComponent } from "./wdknow-index/wdknow-index.component";
import { WdknowCreateComponent } from "./wdknow-create/wdknow-create.component";
import { WdknowEditComponent } from "./wdknow-edit/wdknow-edit.component";

export const WDKNOWLEDGE_COMPONENTS = [
  WdknowIndexComponent,
  WdknowCreateComponent,
  WdknowEditComponent,
];

export { WdknowIndexComponent } from "./wdknow-index/wdknow-index.component";
export { WdknowCreateComponent } from "./wdknow-create/wdknow-create.component";
export { WdknowEditComponent } from "./wdknow-edit/wdknow-edit.component";

export { WdKnowledgeRoutingModule } from "./wdknowledge-routing.module";
