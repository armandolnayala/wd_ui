import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { WdknowIndexComponent } from "./wdknow-index/wdknow-index.component";
import { WdknowCreateComponent } from "./wdknow-create/wdknow-create.component";
import { WdknowEditComponent } from "./wdknow-edit/wdknow-edit.component";

import { AuthGuard } from "../../services/auth.guard";

const wdKnowledgeRoutes: Routes = [
  {
    path: "wdknowledge",
    canActivate: [AuthGuard],
    children: [
      { path: "", redirectTo: "index", pathMatch: "full" },
      { path: "index", component: WdknowIndexComponent },
      { path: "create", component: WdknowCreateComponent },
      { path: "edit/:id/:mode", component: WdknowEditComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(wdKnowledgeRoutes)],
  exports: [RouterModule],
})
export class WdKnowledgeRoutingModule {}
