import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as _ from "underscore";
import { ClipboardService } from "ngx-clipboard";
import { MatChipInputEvent } from "@angular/material/chips";

//Services
import { UserService } from "../../../services/user.service";
import { AlertService } from "../../../services/alert.service";
import { UtilService } from "../../../services/util.service";
import { KnowledgeService } from "../../../services/knowledge.service";
import { ConfirmationdialogService } from "../../../support/services/confirmationdialog.service";

//Models
import { GenericResponse } from "../../../models/genericresponse";
import { OperationResult } from "../../../models/operationresult";
import { WDKnowledge } from "../../../models/wdknowledge";
import { WDKnowEditCreate } from "../../../models/ui/wdknowcreate";
import { Alert } from "../../../models/ui/alertui";
import { ConfirmationdialogModel } from "../../../support/models/ConfirmationdialogModel";
import { TagModel, TagValueModel } from "../../../models/ui/tag-model";

@Component({
  selector: "app-wdknow-create",
  templateUrl: "./wdknow-create.component.html",
  styleUrls: ["./wdknow-create.component.css"],
})
export class WdknowCreateComponent implements OnInit {
  public title: string;
  public genericResponse: GenericResponse;
  public operationResult: OperationResult;
  public wdKnowEntity: WDKnowledge = new WDKnowledge();
  public wdKnowModel: WDKnowEditCreate = new WDKnowEditCreate();
  public tagModel: TagModel = new TagModel();
  public submitExecuted: boolean = false;

  constructor(
    private _wdKnowledgeService: KnowledgeService,
    private _alertService: AlertService,
    private _utilService: UtilService,
    private _confirmationdialogService: ConfirmationdialogService,
    private _router: Router
  ) {
    this.title = "wdknowledge.create_title";
    this.operationResult = new OperationResult(null, "", false);

    this.wdKnowModel.summary = "";
    this.wdKnowModel.description = "";
    this.wdKnowModel.isNotValid = false;

    this.tagModel.selectable = false;
    this.tagModel.removable = true;
    this.tagModel.addOnBlur = false;
    this.tagModel.maxSizeValues = 5;
  }

  ngOnInit() {}

  add(event: MatChipInputEvent): void {
    if (
      this.tagModel.maxSizeValues > 0 &&
      this.tagModel.values.length >= this.tagModel.maxSizeValues
    ) {
      this._alertService.showAlert(
        new Alert("wdknowledge.message_max_tag_values", "warning")
      );
      event.input.value = "";
      return;
    }

    const value = (event.value || "").trim();

    if (value) {
      var tagValue = new TagValueModel();
      tagValue.id = "";
      tagValue.name = value;
      this.tagModel.values.push(tagValue);
    }

    // Clear the input value
    event.input.value = "";
  }

  remove(tagValue: TagValueModel): void {
    const index = this.tagModel.values.findIndex(
      (element) => element.name == tagValue.name
    );

    if (index >= 0) {
      this.tagModel.values.splice(index, 1);
    }
  }

  openExternalLink() {
    if (this.wdKnowModel.url && this.wdKnowModel.url != "") {
      window.open(this.wdKnowModel.url, "_blank");
    }
  }

  confirmationAction() {
    this._confirmationdialogService
      .confirm(ConfirmationdialogModel.defaultDialog())
      .then((confirmed) => {
        if (confirmed == true) {
          this.onSubmit();
        }
      })
      .catch(() => {});
  }

  cancelAction() {
    this._router.navigate(["wdknowledge/index"]);
  }

  validateSubmit() {
    if (!this.wdKnowModel.name || this.wdKnowModel.name == "") {
      this.wdKnowModel.isNotValid = true;
      return false;
    }

    if (!this.wdKnowModel.summary || this.wdKnowModel.summary == "") {
      this.wdKnowModel.isNotValid = true;
      return false;
    }

    this.wdKnowModel.isNotValid = false;
    return true;
  }

  onSubmit() {
    this.operationResult.inProgress = true;
    this.submitExecuted = true;

    if (!this.validateSubmit()) {
      this.operationResult.inProgress = false;
      this._alertService.showAlert(
        new Alert("general.message_data_not_valid", "danger")
      );
      return;
    }

    this.wdKnowModel.tags = this.tagModel.values;
    var entityToSave = WDKnowledge.createFromModel(this.wdKnowModel);

    this._wdKnowledgeService.create(entityToSave).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this._alertService.showAlert(
            new Alert("general.message_action_success", "success")
          );
          this._router.navigate(["wdknowledge/index"]);
        } else {
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, (data) => {
          this.operationResult = data;
        });
      }
    );
  }
}
