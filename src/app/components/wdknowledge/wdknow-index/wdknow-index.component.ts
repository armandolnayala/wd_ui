import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router, ActivatedRoute } from "@angular/router";
import { MatChipInputEvent } from "@angular/material/chips";

//Services
import { UserService } from "../../../services/user.service";
import { AlertService } from "../../../services/alert.service";
import { UtilService } from "../../../services/util.service";
import { KnowledgeService } from "../../../services/knowledge.service";
import { ConfirmationdialogService } from "../../../support/services/confirmationdialog.service";
import { CONSTANT } from "../../../services/constant";

//Models
import {
  PageFilter,
  QueryFilter,
  PopulateFilter,
  GenericFilter,
} from "../../../models/generic-filter";
import { GenericResponse } from "../../../models/genericresponse";
import { OperationResult } from "../../../models/operationresult";
import { WDKnowledge } from "../../../models/wdknowledge";
import { Alert } from "../../../models/ui/alertui";
import { IndexModel } from "../../../models/ui/index-model";
import { Status } from "../../../models/status";
import { ConfirmationdialogModel } from "../../../support/models/ConfirmationdialogModel";
import { TagModel, TagValueModel } from "../../../models/ui/tag-model";

@Component({
  selector: "app-wdknow-index",
  templateUrl: "./wdknow-index.component.html",
  styleUrls: ["./wdknow-index.component.css"],
})
export class WdknowIndexComponent implements OnInit {
  public title: string;
  public indexModel: IndexModel<WDKnowledge> = new IndexModel();

  public genericResponse: GenericResponse;
  public operationResult: OperationResult;
  public queryFilter: QueryFilter;
  public populateFilter: PopulateFilter;
  public filter: GenericFilter;
  public pageFilter: PageFilter;
  public tagModel: TagModel = new TagModel();

  constructor(
    private translate: TranslateService,
    private _userService: UserService,
    private _wdKnowledgeService: KnowledgeService,
    private _alertService: AlertService,
    private _utilService: UtilService,
    private _confirmationdialogService: ConfirmationdialogService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.title = "page.title_wd_knowledge";

    this.populateFilter = new PopulateFilter();
    this.queryFilter = { status: Status.ACTIVE, query: null, extras: [] };
    this.filter = new GenericFilter(this.queryFilter, { name: "asc" });
    this.filter.populateFilter = this.populateFilter;
    this.operationResult = new OperationResult(null, "", false);
    this.pageFilter = new PageFilter(0, CONSTANT.page_limit);

    this.tagModel.selectable = false;
    this.tagModel.removable = true;
    this.tagModel.addOnBlur = false;
    this.tagModel.maxSizeValues = 5;
  }

  ngOnInit() {
    this.translate.use(this._userService.getLang());
    this.loadData();
  }

  loadData() {
    this.operationResult.inProgress = true;

    this._wdKnowledgeService
      .findByFilter(this.filter, this.pageFilter.page, this.pageFilter.limit)
      .subscribe(
        (response) => {
          this.operationResult =
            this._utilService.processGenericResponse(response);

          if (!this.operationResult.error) {
            this.indexModel.result =
              this.operationResult.genericResponse.data.result;
            this.indexModel.hasMore =
              this.operationResult.genericResponse.data.hasMore;
            this.indexModel.count =
              this.operationResult.genericResponse.data.count;
          }
        },
        (httpError) => {
          this._utilService.processHttpError(httpError, (data) => {
            this.operationResult = data;
          });
        }
      );
  }

  reloadResults() {
    this.filter.filter = this.queryFilter;
    this.filter.populateFilter.values =
      this.convertTagValueModelToStringValues();
    this.loadData();
  }

  applyStatusFilter(value: string) {
    this.queryFilter.status = value;
    this.reloadResults();
  }

  onSelectedPageEvent(event) {
    this.pageFilter.page = event.page;
    this.reloadResults();
  }

  onSelectedPageLimitOptionEvent(event) {
    this.pageFilter.limit = event.pageLimit;
    this.reloadResults();
  }

  convertTagValueModelToStringValues() {
    if (!this.tagModel.values || this.tagModel.values.length == 0) {
      return [];
    }

    return this.tagModel.values.map(function (x) {
      return x.getValue().toUpperCase();
    });
  }

  showSection(name: string): Boolean {
    switch (name) {
      case "not_found_results":
        return !this.indexModel.result || this.indexModel.result.length == 0;
      case "results":
        return this.indexModel.result && this.indexModel.result.length > 0;
      case "loading":
        return this.operationResult.inProgress;
      default:
        return false;
    }
  }

  openExternalLink(url: string) {
    if (url) {
      window.open(url, "_blank");
    }
  }

  openConfirmationDialog(itemId: string, operation: string) {
    this._confirmationdialogService
      .confirm(ConfirmationdialogModel.defaultDialog())
      .then((confirmed) => {
        if (confirmed == true) {
          if (operation == "DELETE") {
            this.deleteItem(itemId);
          } else if (operation == "ACTIVATE") {
            this.activateItem(itemId);
          } else if (operation == "INACTIVATE") {
            this.inactivateItem(itemId);
          }
        }
      })
      .catch(() => {});
  }

  private deleteItem(itemId: string) {
    this._wdKnowledgeService.delete(itemId).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this._alertService.showAlert(
            new Alert("general.message_action_success", "success")
          );

          this.reloadResults();
        } else {
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, null);
      }
    );
  }

  private activateItem(itemId: string) {
    this._wdKnowledgeService.activate(itemId).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this._alertService.showAlert(
            new Alert("general.message_action_success", "success")
          );

          this.reloadResults();
        } else {
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, null);
      }
    );
  }

  private inactivateItem(itemId: string) {
    this._wdKnowledgeService.inactivate(itemId).subscribe(
      (response) => {
        this.operationResult =
          this._utilService.processGenericResponse(response);

        if (!this.operationResult.error) {
          this._alertService.showAlert(
            new Alert("general.message_action_success", "success")
          );

          this.reloadResults();
        } else {
          this._alertService.showAlert(
            new Alert(this.operationResult.message, "danger")
          );
        }
      },
      (httpError) => {
        this._utilService.processHttpError(httpError, null);
      }
    );
  }

  navigateToCreate() {
    this.router.navigate(["/wdknowledge/create"]);
  }

  navigateToEdit(id: String) {
    this.router.navigate(["/wdknowledge/edit/" + id + "/1"]);
  }

  onSearchKeyDown(e: any) {
    if (e.keyCode == CONSTANT.key_code.enter && e.ctrlKey) {
      this.processOnSearchKeyDown(true);
    }
    if (e.keyCode == CONSTANT.key_code.space && e.ctrlKey) {
      this.processOnSearchKeyDown(false);
    }
  }

  processOnSearchKeyDown(openUrl) {
    if (this.indexModel.result && this.indexModel.result.length > 0) {
      if (openUrl) {
        if (
          this.indexModel.result[0].url &&
          this.indexModel.result[0].url != null
        ) {
          this.openExternalLink(this.indexModel.result[0].url);
        } else {
          this._alertService.showAlert(
            new Alert("wdproject.edit_message_warning_ctrl_enter", "warning")
          );
        }
      } else {
        this.router.navigate([
          "/wdknowledge/edit/" + this.indexModel.result[0]._id + "/0",
        ]);
      }
    }
  }

  add(event: MatChipInputEvent): void {
    if (
      this.tagModel.maxSizeValues > 0 &&
      this.tagModel.values.length >= this.tagModel.maxSizeValues
    ) {
      this._alertService.showAlert(
        new Alert("wdknowledge.message_max_tag_values", "warning")
      );
      event.input.value = "";
      return;
    }

    const value = (event.value || "").trim();

    if (value) {
      var tagValue = new TagValueModel();
      tagValue.id = "";
      tagValue.name = value;
      this.tagModel.values.push(tagValue);
    }

    // Clear the input value
    event.input.value = "";
  }

  remove(tagValue: TagValueModel): void {
    const index = this.tagModel.values.findIndex(
      (element) => element.name == tagValue.name
    );

    if (index >= 0) {
      this.tagModel.values.splice(index, 1);
    }
  }
}
