import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { CONSTANT } from "../../../services/constant";

@Component({
  selector: "app-app-page-limit",
  templateUrl: "./app-page-limit.component.html",
  styleUrls: ["./app-page-limit.component.css"],
})
export class AppPageLimitComponent implements OnInit {
  public pageLimitOptions = [];
  @Output() onSelectedPageOption = new EventEmitter();

  constructor() {
    this.pageLimitOptions = CONSTANT.page_limit_options;
  }

  ngOnInit() {}

  pageLimitOnChanged(pageLimit) {
    this.sendOnSelectPageOptionEvent(pageLimit);
  }

  sendOnSelectPageOptionEvent(pageLimit) {
    this.onSelectedPageOption.emit({
      pageLimit: pageLimit,
    });
  }
}
