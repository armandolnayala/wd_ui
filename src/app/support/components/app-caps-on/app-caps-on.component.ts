import { Component, HostListener, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-app-caps-on",
  templateUrl: "./app-caps-on.component.html",
  styleUrls: ["./app-caps-on.component.css"],
})
export class AppCapsOnComponent implements OnInit {
  public app_caps_on_capsOn = false;
  public message_caps_on_off = "";

  constructor(private translate: TranslateService) {}

  ngOnInit() {}

  @HostListener("window:click", ["$event"])
  @HostListener("window:keydown", ["$event"])
  @HostListener("window:keyup", ["$event"])
  onCapsCheck(event: MouseEvent | KeyboardEvent) {
    this.app_caps_on_capsOn = !!(
      event.getModifierState && event.getModifierState("CapsLock")
    );

    this.setLabelCapsOnOff();
  }

  setLabelCapsOnOff() {
    let keyToFind = "general.message_caps_on";
    if (!this.app_caps_on_capsOn) {
      keyToFind = "general.message_caps_off";
    }

    this.translate.get(keyToFind).subscribe(
      (data) => {
        this.message_caps_on_off = data;
      },
      (error) => {}
    );
  }
}
