import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ApiService } from "./api.service";

import { GenericResponse } from "../models/genericresponse";
import { WDKnowledge } from "../models/wdknowledge";
import { GenericFilter } from "../models/generic-filter";

import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class KnowledgeService {
  private url: string;
  private serviceUrl: string;

  constructor(private _http: HttpClient, private _apiService: ApiService) {
    this.url = _apiService.apiUrl;
    this.serviceUrl = this.url + "wdknowledge/";
  }

  findByFilter(
    filter: GenericFilter,
    page?: number,
    limit?: number
  ): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.serviceUrl + "find",
      filter,
      this._apiService.getRequestOptions(page, limit)
    );
  }

  findById(id: string): Observable<GenericResponse> {
    return this._http.get<GenericResponse>(
      this.serviceUrl + "find/" + id,
      this._apiService.postRequestOptions()
    );
  }

  create(entity: WDKnowledge): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.serviceUrl + "create",
      entity,
      this._apiService.postRequestOptions()
    );
  }

  update(id: string, data: any): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.serviceUrl + "update/" + id,
      data,
      this._apiService.postRequestOptions()
    );
  }

  delete(id: string): Observable<GenericResponse> {
    return this._http.delete<GenericResponse>(
      this.serviceUrl + "delete/" + id + "/wd-del-1",
      this._apiService.postRequestOptions()
    );
  }

  activate(id: string): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.serviceUrl + "activate/" + id,
      null,
      this._apiService.postRequestOptions()
    );
  }

  inactivate(id: string): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.serviceUrl + "inactivate/" + id,
      null,
      this._apiService.postRequestOptions()
    );
  }

  addWDTag(id: string, entity: any): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.serviceUrl + "wdtag/" + id,
      entity,
      this._apiService.postRequestOptions()
    );
  }

  deleteWDTag(id: string, data: any): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.serviceUrl + "delete-wdtag/" + id,
      data,
      this._apiService.postRequestOptions()
    );
  }
}
