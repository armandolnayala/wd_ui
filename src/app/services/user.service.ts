import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { GLOBAL } from "./global";
import { TranslateService } from "@ngx-translate/core";

import { Credential } from "../models/credential";
import { GenericResponse } from "../models/genericresponse";
import { User } from "../models/user";
import { PasswordChange } from "../models/passwordchange";
import { CreateUser } from "../models/createuser";
import { UpdateUser } from "../models/updateuser";
import { CONSTANT } from "./constant";
import { Observable } from "rxjs";
import { NgbCalendarIslamicUmalqura } from "@ng-bootstrap/ng-bootstrap";

@Injectable({
  providedIn: "root",
})
export class UserService {
  public url: string;
  public activeLang: string;

  constructor(private _http: HttpClient, private _translate: TranslateService) {
    this.url = GLOBAL.url;
    this.activeLang = "en";
  }

  prepareHttpOptions() {
    let httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    };

    httpOptions.headers = httpOptions.headers.set("App-Locale", this.getLang());

    var user = this.getIdentity();
    if (user != null) {
      httpOptions.headers = httpOptions.headers.set(
        "Authorization",
        CONSTANT.jwt_token_prefix + user.token
      );
    }

    return httpOptions;
  }

  signup(credential: Credential): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "user/login",
      credential,
      this.prepareHttpOptions()
    );
  }

  recoveryPassword(emailAddress: String): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "user/recovery-password",
      { email: emailAddress },
      this.prepareHttpOptions()
    );
  }

  changePassword(passwordChange: PasswordChange): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "user/change-password",
      {
        email: passwordChange.email,
        password: passwordChange.password,
        code: passwordChange.code,
      },
      this.prepareHttpOptions()
    );
  }

  confirmUser(id: string, code: string): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.url + "user/confirm-user" + "/" + id + "/" + code,
      {},
      this.prepareHttpOptions()
    );
  }

  createUser(createUser: CreateUser): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "user/create",
      {
        name: createUser.name,
        surname: createUser.surname,
        email: createUser.email,
        password: createUser.password,
        locale: createUser.locale,
      },
      this.prepareHttpOptions()
    );
  }

  updateUser(updateUser: UpdateUser): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.url + "user/update",
      updateUser,
      this.prepareHttpOptions()
    );
  }

  findById(id: String): Observable<GenericResponse> {
    return this._http.get<GenericResponse>(
      this.url + "user/find/" + id,
      this.prepareHttpOptions()
    );
  }

  deleteAccount(wdDataKey: string): Observable<GenericResponse> {
    var httpOptions = this.prepareHttpOptions();

    httpOptions.headers = httpOptions.headers.set(
      "wd-data-key",
      btoa(wdDataKey)
    );

    return this._http.delete<GenericResponse>(
      this.url + "user/delete-account/wd-del-1",
      httpOptions
    );
  }

  saveIdentity(identity) {
    let user = new User("", "", "", false, "", "", "");

    user.name = identity.data.user.name;
    user.surname = identity.data.user.surname;
    user.email = identity.data.user.email;
    user.isAdmin = identity.data.user.isAdmin;
    user.token = identity.data.token;
    user.locale = identity.data.user.locale;
    user.id = identity.data.user.id;
    user.secret = this.setUserSecret(user);

    if (user.locale) {
      this.setLang(user.locale);
    }

    localStorage.setItem(GLOBAL.keyIdentity, JSON.stringify(user));
  }

  getIdentity(): User {
    let jsonUser = localStorage.getItem(GLOBAL.keyIdentity);

    if (!jsonUser) {
      return null;
    }

    let user = JSON.parse(jsonUser);
    if (user != "undefined") {
      return user;
    }

    return null;
  }

  getLang(): string {
    return this.activeLang;
  }

  setLang(lang: string): string {
    this.activeLang = lang;
    return this.activeLang;
  }

  updateIdentity(data) {
    this.setLang(data.locale);
    var userIdentity = this.getIdentity();
    userIdentity.locale = data.locale;
    localStorage.setItem(GLOBAL.keyIdentity, JSON.stringify(userIdentity));
    this._translate.use(data.locale);
  }

  logout() {
    localStorage.clear();
  }

  setUserSecret(user) {
    var userSecret = user.id + "_@_" + user.email;
    for (var j = 0; j < CONSTANT.encode_iterations; j++) {
      userSecret = Buffer.from(userSecret).toString('base64');
    }

    return userSecret;
  }

  getUserSecret() {
    var user = this.getIdentity();

    if (user) {
      return user.secret;
    }

    return null;
  }
}
