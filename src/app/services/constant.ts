//Variable Global para usar en la App
export var CONSTANT = {
  locales: [
    {
      label: "Spanish",
      lang: "es",
    },
    {
      label: "English",
      lang: "en",
    },
    {
      label: "Portuguese",
      lang: "pt",
    },
  ],
  page_limit: 15,
  jwt_token_prefix: "Bearer ",
  key_code: {
    enter: 13,
    space: 32,
  },
  page_limit_options: [
    {
      id: 15,
      label: "15",
    },
    {
      id: 25,
      label: "25",
    },
    {
      id: 50,
      label: "50",
    },
    {
      id: 100,
      label: "100",
    },
  ],
  encode_iterations: 3
};
