import { Injectable } from "@angular/core";
import { HttpClient, JsonpClientBackend } from "@angular/common/http";
import { ApiService } from "./api.service";

import { GenericResponse } from "../models/genericresponse";
import { WDProject } from "../models/wdproject";
import { GenericFilter } from "../models/generic-filter";

import { CONSTANT } from "./constant";
import { Observable } from "rxjs";
import { WDData } from "../../../../../wd-github/workdesk_ui/src/app/models/wdproject";

@Injectable({
  providedIn: "root",
})
export class WdprojectService {
  private url: string;

  constructor(private _http: HttpClient, private _apiService: ApiService) {
    this.url = _apiService.apiUrl;
  }

  findAll(
    filter: GenericFilter,
    page?: number,
    limit?: number
  ): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "wdproject/find",
      filter,
      this._apiService.getRequestOptions(page, limit)
    );
  }

  findById(id: string): Observable<GenericResponse> {
    return this._http.get<GenericResponse>(
      this.url + "wdproject/find/" + id,
      this._apiService.postRequestOptions()
    );
  }

  create(entity: WDProject): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "wdproject/create",
      entity,
      this._apiService.postRequestOptions()
    );
  }

  update(id: string, data: any): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.url + "wdproject/update/" + id,
      data,
      this._apiService.postRequestOptions()
    );
  }

  delete(id: string): Observable<GenericResponse> {
    return this._http.delete<GenericResponse>(
      this.url + "wdproject/delete/" + id + "/wd-del-1",
      this._apiService.postRequestOptions()
    );
  }

  activate(id: string): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.url + "wdproject/activate/" + id,
      null,
      this._apiService.postRequestOptions()
    );
  }

  inactivate(id: string): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.url + "wdproject/inactivate/" + id,
      null,
      this._apiService.postRequestOptions()
    );
  }

  addWDData(id: string, entity: any): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "wdproject/wddata/" + id,
      entity,
      this._apiService.postRequestOptions()
    );
  }

  updateWDData(
    id: string,
    wddid: string,
    entity: any
  ): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.url + "wdproject/wddata/" + id + "/" + wddid,
      entity,
      this._apiService.postRequestOptions()
    );
  }

  deleteWDData(id: string, data: any): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "wdproject/delete-wddata/" + id,
      data,
      this._apiService.postRequestOptions()
    );
  }

  decodeWDData(id: string, entity: any): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "wdproject/decode-wddata/" + id,
      entity,
      this._apiService.postRequestOptions()
    );
  }

  decodeWDDataToShow(wdDataValues: WDData[]): WDData[] {
    var wdDataResult = [];

    for (var i = 0; i < wdDataValues.length; i++) {
      let entityWDData = wdDataValues[i];

      if (entityWDData.encode) {
        let decodedValueToProcess = Buffer.from(
          entityWDData.value,
          "base64"
        ).toString("ascii");
        let decodedValueAndSecret = decodedValueToProcess.split("_WD_");
        if (decodedValueAndSecret[1] !== this._apiService.getUserSecret()) {
          throw "Validation encode value error";
        }

        let decodedValue = decodedValueAndSecret[0];

        for (var j = 0; j < CONSTANT.encode_iterations; j++) {
          decodedValue = Buffer.from(decodedValue, "base64").toString("ascii");
        }

        entityWDData.value = decodedValue;
      }

      wdDataResult = [...wdDataResult, entityWDData];
    }

    return wdDataResult;
  }
}
