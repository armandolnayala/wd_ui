import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { UserService } from "./user.service";
import { ApiService } from "./api.service";

import { GenericResponse } from "../models/genericresponse";
import { Accesslink } from "../models/accesslink";
import { GenericFilter } from "../models/generic-filter";

import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AccesslinkService {
  public url: string;

  constructor(
    private _http: HttpClient,
    private _userService: UserService,
    private _apiService: ApiService
  ) {
    this.url = _apiService.apiUrl;
  }

  findAll(
    filter: GenericFilter,
    page?: number,
    limit?: number
  ): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "accesslink/find",
      filter,
      this._apiService.getRequestOptions(page, limit)
    );
  }

  createAccessLink(accessLink: Accesslink): Observable<GenericResponse> {
    return this._http.post<GenericResponse>(
      this.url + "accesslink/create",
      accessLink,
      this._userService.prepareHttpOptions()
    );
  }

  updateAccessLink(id: string, data: any): Observable<GenericResponse> {
    return this._http.put<GenericResponse>(
      this.url + "accesslink/update/" + id,
      data,
      this._apiService.postRequestOptions()
    );
  }

  deleteAccessLink(accessLinkId: string): Observable<GenericResponse> {
    return this._http.delete<GenericResponse>(
      this.url + "accesslink/delete/" + accessLinkId,
      this._userService.prepareHttpOptions()
    );
  }
}
