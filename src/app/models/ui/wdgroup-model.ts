import { WDABMBaseModel } from "./wdbase-model";

export class WDGroupEditCreate extends WDABMBaseModel {
  public _id: string;
  public name: string;
  public canBeRemoved: Boolean;

  constructor() {
    super();
  }

  static getInstance(pName: string, pCanBeRemoved: Boolean): WDGroupEditCreate {
    var entity = new WDGroupEditCreate();
    entity.name = pName;
    entity.canBeRemoved = pCanBeRemoved;

    return entity;
  }
}
