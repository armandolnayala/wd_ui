import { COMMA, ENTER } from "@angular/cdk/keycodes";

export class TagValueModel {
  public id: string;
  public name: string;

  constructor() {}

  static instanceToSave(pId: string, pValue: string): TagValueModel {
    var entity = new TagValueModel();
    entity.id = pId;
    entity.name = pValue;

    return entity;
  }

  getValue() {
    return this.name;
  }
}

export class TagModel {
  public separatorKeysCodes = [ENTER, COMMA] as const;

  public selectable: boolean = false;
  public removable: boolean = false;
  public addOnBlur: boolean = false;
  public values: TagValueModel[] = [];
  public maxSizeValues: number = 0;

  constructor() {}
}
