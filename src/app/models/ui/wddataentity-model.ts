import { WDABMBaseModel } from "./wdbase-model";
import { WDGroupEditCreate } from "./wdgroup-model";

export class WDDataEntityEditCreate extends WDABMBaseModel {
  public id: string;
  public name: string;
  public value: string;
  public url: string;
  public encode: Boolean;
  public description: string;
  public group: WDGroupEditCreate = new WDGroupEditCreate();

  constructor() {
    super();
  }

  static getInstance(
    pName: string,
    pValue: string,
    pUrl: string,
    pEncode: Boolean,
    pDescription: string
  ): WDDataEntityEditCreate {
    var entity = new WDDataEntityEditCreate();
    entity.name = pName;
    entity.value = pValue;
    entity.url = pUrl;
    entity.encode = pEncode;
    entity.description = pDescription;
    entity.isNotValid = false;
    return entity;
  }
}
