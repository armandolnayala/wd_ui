import { TagValueModel } from "./tag-model";

export class WDTagEditCreate {
  public _id: string;
  public value: string;

  constructor() {}

  static getInstance(pValue: string): WDTagEditCreate {
    var entity = new WDTagEditCreate();
    entity.value = pValue;

    return entity;
  }
}

export class WDKnowEditCreate {
  public id: string;
  public name: string;
  public url: string;
  public description: string;
  public summary: string;
  public isNotValid: boolean = false;
  public isReadOnlyMode: boolean = false;
  public tags: TagValueModel[] = [];

  constructor() {}

  static getInstance(
    pName: string,
    pUrl: string,
    pDescription: string,
    pSummary: string
  ): WDKnowEditCreate {
    var entity = new WDKnowEditCreate();
    entity.name = pName;
    entity.url = pUrl;
    entity.description = pDescription;
    entity.summary = pSummary;
    entity.isNotValid = false;
    return entity;
  }
}
