export class UpdateDataUser {
  public name: String;
  public surname: String;
  public email: String;
  public password: String;
  public locale: String;

  constructor() {}
}

export class UpdateUser {
  public user: UpdateDataUser;
  public updatePassword: Boolean = false;
  public currentPassword: String;

  constructor() {}
}

export class UpdateUserUI {
  public id: String;
  public name: String;
  public surname: String;
  public email: String;
  public password: String;
  public confirmPassword: String;
  public currentPassword: String;
  public locale: String;
  public updatePassword: Boolean = false;
  public locales = [];

  constructor() {}
}
