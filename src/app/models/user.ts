export class User {
  public secret: string;
  constructor(
    public name: string,
    public surname: string,
    public email: string,
    public isAdmin: boolean,
    public token: string,
    public locale: string,
    public id: string
  ) {}
}
