import { WDKnowEditCreate } from "./ui/wdknowcreate";

export class WDTag {
  public _id: string;
  public value: string;

  constructor() {}

  static instanceToSave(pValue: string): WDTag {
    var entity = new WDTag();
    entity.value = pValue;

    return entity;
  }
}

export class WDKnowledge {
  public _id: string;
  public name: string;
  public url: string;
  public description: string;
  public summary: string;
  public status: string;
  public tagValues: WDTag[] = [];
  public createdDate: Date;
  public createdUnix: Number;
  public updatedDate: Date;
  public updatedUnix: Number;

  constructor() {}

  static instanceToSave(
    pName: string,
    pUrl: string,
    pDescription: string,
    pSummary: string
  ): WDKnowledge {
    var entity = new WDKnowledge();
    entity.name = pName;
    entity.url = pUrl;
    entity.description = pDescription;
    entity.summary = pSummary;

    return entity;
  }

  static createFromModel(pWDKnowEditCreate: WDKnowEditCreate): WDKnowledge {
    var entity = new WDKnowledge();
    entity.name = pWDKnowEditCreate.name;
    entity.summary = pWDKnowEditCreate.summary;
    entity.url = pWDKnowEditCreate.url;
    entity.description = pWDKnowEditCreate.description;
    entity.tagValues = pWDKnowEditCreate.tags.map(function (x) {
      return WDTag.instanceToSave(x.name);
    });

    return entity;
  }
}
