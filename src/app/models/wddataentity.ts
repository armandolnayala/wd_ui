import { WDDataEntityEditCreate } from "./ui/wddataentity-model";

export class WDGroup {
  public _id: string;
  public name: string;
  public canBeRemoved: Boolean;
  public createdDate: Date;
  public createdUnix: Number;
  public updatedDate: Date;
  public updatedUnix: Number;

  constructor() {}

  static instanceToSave(pName: string, pCanBeRemoved: Boolean): WDGroup {
    var entity = new WDGroup();

    entity.name = pName;
    entity.canBeRemoved = pCanBeRemoved;

    return entity;
  }
}

export class WDDataEntity {
  public _id: string;
  public name: string;
  public value: string;
  public url: string;
  public encode: Boolean;
  public description: string;
  public group: WDGroup = new WDGroup();
  public createdDate: Date;
  public createdUnix: Number;
  public updatedDate: Date;
  public updatedUnix: Number;

  constructor() {}

  static instanceToSave(
    pName: string,
    pValue: string,
    pUrl: string,
    pEncode: Boolean,
    pDescription: string,
    pGroupId: string
  ): WDDataEntity {
    var entity = new WDDataEntity();
    entity.name = pName;
    entity.value = pValue;
    entity.url = pUrl;
    entity.encode = pEncode;
    entity.description = pDescription;
    entity.group._id = pGroupId;

    return entity;
  }

  static createFromModel(
    pWDDataEntityEditCreate: WDDataEntityEditCreate
  ): WDDataEntity {
    var entity = new WDDataEntity();
    entity.name = pWDDataEntityEditCreate.name;
    entity.value = pWDDataEntityEditCreate.value;
    entity.url = pWDDataEntityEditCreate.url;
    entity.encode = pWDDataEntityEditCreate.encode;
    entity.description = pWDDataEntityEditCreate.description;
    entity.group._id = pWDDataEntityEditCreate.group._id;

    return entity;
  }
}
