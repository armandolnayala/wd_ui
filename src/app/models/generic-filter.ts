export class PageFilter {
  constructor(public page: number, public limit: number) {}
}

export class QueryFilter {
  status: string;
  query: string;
  extras: Object[];
}

export class PopulateFilter {
  public values: string[] = [];
  public value: string = null;

  constructor() {}
}

export class GenericFilter {
  public populateFilter: PopulateFilter;

  constructor(public filter: QueryFilter, public sort: any) {}
}
